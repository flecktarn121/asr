Small repository for the practices of **Administration of Systems and Networks**.

The report is written in [RMarkdown](https://rmarkdown.rstudio.com/). To install the necessary dependencies first you need

    r pandoc-citeproc
    
Then, once you enter in R (try to do it as root to have it installed for all users), run

    install.packages('rmarkdown', repos='http://cran.us.r-project.org')

And finally you will need latex

    texlive-core
    
Once everything is installed, just run

    sh compile.sh
    
to generate the doument.

For more information, check [Luke Smith's video](https://www.youtube.com/watch?v=4J5a0JWIF-0) .